package support;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BasePage {
    protected WebDriver driver;

    //instancia webdriver
    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public static WebDriver create_chrome() {

        System.setProperty("WebDriver.Chrome.Driver", "C:\\tools\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("https://www.grocerycrud.com/demo/bootstrap_theme");
        return driver;

    }

}
