package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import support.BasePage;

public class CustomerPage extends BasePage {

    public CustomerPage(WebDriver driver) {
        super(driver);
    }

    // Clica no primeiro Checkbox
    public CustomerPage chk_customer() throws InterruptedException {
        driver.findElement(By.xpath("//*[@class='select-row']")).click();
        return this;
    }

    // Clica no botão delete
    public CustomerPage btn_delete_customer() throws InterruptedException {
        driver.findElement(By.xpath("//*[@class='btn btn-outline-dark delete-selected-button']")).click();
        Thread.sleep(3000);
        return this;

    }

    // Clica no botão delete para confirmar
    public CustomerPage btn_confirm_delete() throws InterruptedException {
        driver.findElement(By.xpath("//*[@class='btn btn-danger delete-multiple-confirmation-button']")).click();
        Thread.sleep(3000);
        return this;
    }

    // Mensagem alert de confirmando delete
    public String msg_delete() {
        return driver.findElement(By.xpath("//*[@class='alert-delete-multiple-one']")).getText();
    }


    // Pop up aparece após confirmar delete
    public String msg_success_delete() {
        return driver.findElement(By.xpath("//*[@class='alert alert-success growl-animated animated bounceInDown']")).getText();
    }


}
