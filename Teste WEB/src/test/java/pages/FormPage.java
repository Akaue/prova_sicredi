package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import support.BasePage;


public class FormPage extends BasePage {
    public FormPage(WebDriver driver) {
        super(driver);
    }

    // Preenche formulário
    public FormPage fill_form() throws InterruptedException {
        //        Name: Teste Sicredi
        Thread.sleep(1000);
        driver.findElement(By.id("field-customerName")).sendKeys("Teste Sicredi");
        //        Last name: Teste
        driver.findElement(By.id("field-contactLastName")).sendKeys("Teste");
        //        ContactFirstName: seu nome
        driver.findElement(By.id("field-contactFirstName")).sendKeys("Akaue Lima");
        //        Phone: 51 9999-9999
        driver.findElement(By.id("field-phone")).sendKeys("51 9999-9999");
        //        AddressLine1: Av Assis Brasil, 3970
        driver.findElement(By.id("field-addressLine1")).sendKeys(" Av Assis Brasil, 3970");
        //        AddressLine2: Torre D
        driver.findElement(By.id("field-addressLine2")).sendKeys("Torre D");
        //        City: Porto Alegre
        driver.findElement(By.id("field-city")).sendKeys("Porto Alegre");
        //        State: RS
        driver.findElement(By.id("field-state")).sendKeys("RS");
        //        PostalCode: 91000-000
        driver.findElement(By.id("field-postalCode")).sendKeys("91000-000");
        //        Country: Brasil
        driver.findElement(By.id("field-country")).sendKeys("Brasil");
        //        from Employeer: Fixter
        driver.findElement(By.xpath("//a[@class='chosen-single chosen-default']")).click();
        //        CreditLimit: 200
        driver.findElement(By.id("field-creditLimit")).sendKeys("200");
        return this;
    }

    // Clica no botão salvar formulário preenchido
    public FormPage btnSave_form() throws InterruptedException {
        driver.findElement(By.id("form-button-save")).click();
        Thread.sleep(3000);
        return this;
    }

    // Clica no botão salva formulario vazio
    public FormPage btnSave_form2() throws InterruptedException {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("javascript:window.scrollBy(250,350)");
        Thread.sleep(3000);
        driver.findElement(By.id("form-button-save")).click();
        return this;
    }

    // Mensagem de sucesso após preencher formulário
    public String msg_success() {
        return driver.findElement(By.xpath("//*[@class=\"form-group gcrud-form-group\"]")).getText();
    }

    // Clica no botão Go back to list
    public CustomerPage btnGo_back() throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.xpath("//a[contains(text(),'Go back to list')]")).click();
        return new CustomerPage(driver);
    }


}
