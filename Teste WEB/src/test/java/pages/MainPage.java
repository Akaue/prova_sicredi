package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import support.BasePage;


public class MainPage extends BasePage {

    public MainPage(WebDriver driver) {
        super(driver);
    }

    // Altera a versão para Bootstrap V4 Theme
    public MainPage change_combo_select() {
        Select select = new Select(driver.findElement(By.id("switch-version-select")));
        select.selectByVisibleText("Bootstrap V4 Theme");
        return this;
    }

    // Preenche o nome do usuário que foi cadastrado
    public MainPage fill_name(String customer) throws InterruptedException {
        driver.findElement(By.xpath("//*[@name='customerName']")).sendKeys(customer);
        Thread.sleep(5000);
        return this;
    }

    // Clica no botão adiciona usuário
    public FormPage btnAdd_costumer() {
        driver.findElement(By.xpath("//i[@class='el el-plus']")).click();
        return new FormPage(driver);
    }
}
