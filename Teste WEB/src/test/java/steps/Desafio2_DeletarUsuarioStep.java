package steps;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.CustomerPage;
import pages.FormPage;
import pages.MainPage;
import support.BasePage;


public class Desafio2_DeletarUsuarioStep {

    private WebDriver driver;

    @Before
    public void setUp() {
        driver = BasePage.create_chrome();
    }

    @Test
    public void DeletarUsuarioTest() throws InterruptedException {
        MainPage mainPage = new MainPage(driver);
        FormPage formPage = new FormPage(driver);
        CustomerPage customerPage = new CustomerPage(driver);

//  1. Clicar no link Go back to list
        mainPage.change_combo_select();
        mainPage.btnAdd_costumer();
        formPage.btnSave_form2();
        formPage.btnGo_back();

//  2. Clicar na coluna “Search Name” e digitar o conteúdo do Name (Teste Sicredi)
        mainPage.fill_name("Teste Sicredi");

//  3. Clicar no checkbox abaixo da palavra Actions
        customerPage.chk_customer();

//  4. Clicar no botão Delete
        customerPage.btn_delete_customer();

//  5. Validar o texto “Are you sure that you want to delete this 1 item?” através de uma asserção para a popup que será apresentada
        String message_delete = "Are you sure that you want to delete this 1 item?";
        Assert.assertEquals(message_delete, customerPage.msg_delete());

//  6. Clicar no botão Delete da popup, aparecerá uma mensagem dentro de um box verde na parte superior direito da tela.
        customerPage.btn_confirm_delete();


//Adicione uma asserção na mensagem “Your data has been successfully deleted from the database.”
        /*
        HOUVE UM PROBLEMA, nessa etapa do teste, ele realiza o click no botão delete, porém a tela permanece,
        não fechando, o que impede a devida validação.

         String message_success_delete = "Your data has been successfully deleted from the database.";
         Assert.assertEquals(message_success_delete, costumerPage.msg_success_delete());
        */
    }

    //  7. Fechar o browser
    @After
    public void tearDown() {
        driver.quit();
    }


}
