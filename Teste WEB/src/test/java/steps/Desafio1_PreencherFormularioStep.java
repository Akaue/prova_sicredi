package steps;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import pages.FormPage;
import pages.MainPage;
import support.BasePage;

public class Desafio1_PreencherFormularioStep {
    private WebDriver driver;


    @Before
    public void setUp() {
//  1. Acessar a página https://www.grocerycrud.com/demo/bootstrap_theme
        driver = BasePage.create_chrome();
    }

    @Test
    public void PreencherFormularioTest() throws InterruptedException {
        MainPage mainPage = new MainPage(driver);
        FormPage formPage = new FormPage(driver);

//  2. Mudar o valor da combo Select version para “Bootstrap V4 Theme”
        mainPage.change_combo_select();

//  3. Clicar no botão Add Customer
        mainPage.btnAdd_costumer();

//  4. Preencher os campos do formulário com as seguintes informações:
        formPage.fill_form();

//  5. Clicar no botão Save
        formPage.btnSave_form();

//  6. Validar a mensagem “Your data has been successfully stored into the database. Edit Customer or Go back to list” através de uma asserção
        String message = "Your data has been successfully stored into the database. Edit Customer or Go back to list";
        Assert.assertEquals(message, formPage.msg_success());
    }

    //  7. Fechar o browser
    @After
    public void tearDown() {
        driver.quit();
    }
}