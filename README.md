# PROVA SICREDI

## Testes Web e API.  Testes realizados seguindo as atividades propostas na prova da Sicredi fornecida pela IBM.

## Teste Api

##### Api usada: https://viacep.com.br
`
Utilizei a IDE intellij, os frameworks REST Assured e Junit
`


## Teste Web

##### Site usado: https://www.grocerycrud.com/demo/bootstrap_theme

`
Utilizei a IDE intellij, os frameworks Selenium WebDriver e Junit
`

###### No Desafio 2, ao final, não foi possivel realizar a devida validação, pois, após clicar no botão delete, o mesmo não toma nenhuma ação, oque impossibilita a validação final.
