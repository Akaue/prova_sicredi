package steps;

import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class TesteApiViaCep {

    @Test
    public void ConsultaCEPValidoTest() {

//  Dado que o usuário inseri um CEP válido
        String url = "https://viacep.com.br/ws/91060900/json/";
        Response response = given().contentType("application/json")

//  Quando o serviço é consultado
                .when().get(url);
//  Então é retornado o CEP,logradouro, complemento, bairro, localidade, uf e ibge
        response.then().statusCode(200);
        System.out.println("Response code: " + response.statusCode());
        System.out.println("Response body: " + response.body().asString());

        Assert.assertEquals(200, response.statusCode());
    }

    @Test
    public void ConsultaCEPInexistenteTest() {

//  Dado que o usuário inseri um CEP que não exista na base dos correios
        String url = "https://viacep.com.br/ws/91060966/json/";
        Response response = given().contentType("application/json")

//  Quando o serviço é consultado
                .when().get(url);
//  Então é retornado um atributo erro
        response.then().statusCode(200);
        System.out.println("Response code: " + response.statusCode());
        System.out.println("Response body: " + response.body().asString());

        Assert.assertEquals(200, response.statusCode());

    }

    @Test
    public void ConsultaCEPFormatoInvalidoTest() {

//  Dado que o usuário inseri um CEP com formato inválido
        String url = "https://viacep.com.br/ws/910TgHii/json/";
        Response response = given().contentType("application/json")

//  Quando o serviço é consultado
                .when().get(url);
//  Então é retornado uma mensagem de erro
        response.then().statusCode(400);
        System.out.println("Response code: " + response.statusCode());
        System.out.println("Response body: " + response.body().asString());

        Assert.assertEquals(400, response.statusCode());

    }


    @Test
    public void ConsultaParteLogradouroTest() {

//  Dado que o usuário inseri a parte de um nome de logradouro
        String url = "https://viacep.com.br/ws/RS/Gravatai/Barroso/json/";
        Response response = given().contentType("application/json")

//  Quando o serviço é consultado
                .when().get(url);
//  Então é retornado o corpo de todos os endereços com esse nome
        response.then().statusCode(200);
        System.out.println("Response code: " + response.statusCode());
        System.out.println("Response body: " + response.body().asString());

        Assert.assertEquals(200, response.statusCode());
    }

}